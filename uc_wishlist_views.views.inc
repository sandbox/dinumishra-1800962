<?php

/**
 * @file
 * Views 2 hooks and callback registries.
 */

/**
 * Implements hook_views_data().
 */
function uc_wishlist_views_views_data() {

  $data['uc_products']['addtowishlist'] = array(
    'title' => t('UC wishlist button'),
    'help' => t('A button to add a product to the wishlist.'),
    'group' => t('UC Wishlist'),
    'field' => array(
      'table' => 'node',
      'additional fields' => array(
        'nid' => array(
          'table' => 'node',
          'field' => 'nid',
        ),
      ),
      'handler' => 'uc_wishlist_handler_field_addtowishlist',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_handlers().
 */
function uc_wishlist_views_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'uc_wishlist_views'),
    ),
    'handlers' => array(
      'uc_wishlist_handler_field_addtowishlist' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}
